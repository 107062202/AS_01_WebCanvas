# Software Studio 2020 Spring
## Assignment 01 Web Canvas


### Scoring

| **Basic components**                             | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Basic control tools                              | 30%       | Y         |
| Text input                                       | 10%       | Y         |
| Cursor icon                                      | 10%       | Y         |
| Refresh button                                   | 10%       | Y         |

| **Advanced tools**                               | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Different brush shapes                           | 15%       | Y         |
| Un/Re-do button                                  | 10%       | Y         |
| Image tool                                       | 5%        | Y         |
| Download                                         | 5%        | Y         |


---

### Gitlab page link

https://107062202.gitlab.io/AS_01_WebCanvas

### How to use 

#### 外觀

![](https://i.imgur.com/yzzWJgF.png)

#### 功能介紹

![](https://i.imgur.com/A3sWMi4.png)

    調色盤
    
![](https://i.imgur.com/COr42gs.png)
    
    各種功能

![](https://i.imgur.com/sRVz1kZ.png)
    
    調整大小

<style>
table th{
    width: 100%;
}
</style>