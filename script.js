let canvas = document.getElementById("drawing-board");
let ctx = canvas.getContext("2d");
let eraser = document.getElementById("eraser");
let brush = document.getElementById("brush");
let text = document.getElementById("text");
let circle = document.getElementById("circle");
let triangle = document.getElementById("triangle");
let rectangle = document.getElementById("rectangle");
let reSetCanvas = document.getElementById("clear");
let aColorBtn = document.getElementsByClassName("color-item");
let save = document.getElementById("save");
let load = document.getElementById("upload");
let undo = document.getElementById("undo");
let redo = document.getElementById("redo");
let range = document.getElementById("range");
let clear = false;
let activeColor = 'lightcoral';
let lWidth = 4;
let textFont = document.getElementById("font");
let fontFamily = "sans-serif";

let input_text = document.getElementById("textinput");
let historyData = [];
let undoData = [];
let mode = "drawline";

autoSetSize(canvas);

setCanvasBg('#ddd');

listenToUser(canvas);

getColor();

window.onbeforeunload = function () {
    return "Reload site?";
};

function autoSetSize(canvas) {
    canvasSetSize();

    function canvasSetSize() {
        let pageWidth = document.documentElement.clientWidth;
        let pageHeight = document.documentElement.clientHeight;

        canvas.width = pageWidth;
        canvas.height = pageHeight;
    }

    window.onresize = function () {
        canvasSetSize();
    }
}

function setCanvasBg(color) {
    ctx.fillStyle = color;
    ctx.fillRect(0, 0, canvas.width, canvas.height);
    ctx.fillStyle = "black";
}

function listenToUser(canvas) {
    let painting = false;
    let circle = false;
    let triangle = false;
    let rectangle = false;

    let lastPoint = {
        x: undefined,
        y: undefined
    };

    this.firstDot = ctx.getImageData(0, 0, canvas.width, canvas.height); //在这里储存绘图表面
    saveData(this.firstDot);
    canvas.onmousedown = function (e) {
        if (mode == "text") {
            input_text.style.top = e.clientY + "px";
            input_text.style.left = e.clientX + "px";
            input_text.value = "";
            input_text.style.display = "initial";
            input_text.focus();
        } else {
            if (mode == "drawline" || mode == "eraser") {
                painting = true;
                let x = e.clientX;
                let y = e.clientY;
                lastPoint = {
                    "x": x,
                    "y": y
                };
                ctx.save();
                drawCircle(x, y, 0);
            } else if (mode == "drawcircle") {
                circle = true;
                let x = e.clientX;
                let y = e.clientY;
                lastPoint = {
                    "x": x,
                    "y": y
                };
                ctx.save();
                drawCircle(x, y, 0);
            } else if (mode == "drawtriangle") {
                triangle = true;
                let x = e.clientX;
                let y = e.clientY;
                lastPoint = {
                    "x": x,
                    "y": y
                };
                ctx.save();
                drawCircle(x, y, 0);
            } else if (mode == "drawrectangle") {
                rectangle = true;
                let x = e.clientX;
                let y = e.clientY;
                lastPoint = {
                    "x": x,
                    "y": y
                };
                ctx.save();
                drawCircle(x, y, 0);
            }
        }
    };
    canvas.onmousemove = function (e) {
        let x = e.clientX;
        let y = e.clientY;
        let newPoint = {
            "x": x,
            "y": y
        };
        if (painting) {
            drawLine(lastPoint.x, lastPoint.y, newPoint.x, newPoint.y, clear);
            lastPoint = newPoint;
        } else if (circle) {
            ctx.putImageData(historyData[historyData.length - 1], 0, 0);
            drawCircle2(lastPoint.x, lastPoint.y, newPoint.x, newPoint.y);
        } else if (triangle) {
            ctx.putImageData(historyData[historyData.length - 1], 0, 0);
            drawTriangle(lastPoint.x, lastPoint.y, newPoint.x, newPoint.y);
        } else if (rectangle) {
            ctx.putImageData(historyData[historyData.length - 1], 0, 0);
            drawRectangle(lastPoint.x, lastPoint.y, newPoint.x, newPoint.y);
        }

        canvas.onmouseup = function () {
            painting = false;
            circle = false;
            triangle = false;
            rectangle = false;
            this.firstDot = ctx.getImageData(0, 0, canvas.width, canvas.height); //在这里储存绘图表面
            saveData(this.firstDot);
        };

        canvas.mouseleave = function () {
            painting = false;
        }
    }
}

function changeFont() {
    let index = textFont.selectedIndex;
    fontFamily = textFont.options[index].value;
}

function drawText(event) {
    if (event.key != "Enter")
        return;
    let value = input_text.value;
    let x = input_text.style.left.slice(0, -2);
    let y = input_text.style.top.slice(0, -2);

    ctx.fillStyle = activeColor;
    ctx.font = lWidth * 2 + "px " + fontFamily;
    ctx.fillText(value, x, y);
    input_text.style.display = "none";
    this.firstDot = ctx.getImageData(0, 0, canvas.width, canvas.height); //在这里储存绘图表面
    saveData(this.firstDot);
}

function drawCircle(x, y, radius) {
    ctx.save();
    ctx.beginPath();
    ctx.arc(x, y, radius, 0, Math.PI * 2);
    ctx.fill();
    if (clear) {
        ctx.clip();
        ctx.clearRect(0, 0, canvas.width, canvas.height);
        ctx.restore();
    }
}

function drawLine(x1, y1, x2, y2) {
    ctx.lineWidth = lWidth;
    ctx.lineCap = "round";
    ctx.lineJoin = "round";
    if (clear) {
        ctx.save();
        ctx.globalCompositeOperation = "destination-out";
        ctx.moveTo(x1, y1);
        ctx.lineTo(x2, y2);
        ctx.stroke();
        ctx.closePath();
        ctx.clip();
        ctx.clearRect(0, 0, canvas.width, canvas.height);
        ctx.restore();
        console.log("QAQ");
    } else {
        ctx.moveTo(x1, y1);
        ctx.lineTo(x2, y2);
        ctx.stroke();
        ctx.closePath();
    }
}

function drawCircle2(x1, y1, x2, y2) {
    ctx.lineWidth = lWidth;
    var r;
    if ((x2 - x1) > (y1 - y2))
        r = Math.abs((y2 - y1) / 2);
    else
        r = Math.abs((x2 - x1) / 2);

    var Ox = (x2 > x1) ? x1 + r : x1 - r;
    var Oy = (y2 > y1) ? y1 + r : y1 - r;
    ctx.beginPath();
    ctx.arc(Ox, Oy, r, 0, 2 * Math.PI);
    ctx.stroke();
}

function drawTriangle(x1, y1, x2, y2) {
    ctx.lineWidth = lWidth;
    ctx.beginPath();
    ctx.moveTo(x1 + (x2 - x1) / 2, y1);
    ctx.lineTo(x2, y2);
    ctx.lineTo(x1, y2);
    ctx.closePath();
    ctx.stroke();
}

function drawRectangle(x1, y1, x2, y2) {
    ctx.lineWidth = lWidth;
    ctx.beginPath();
    ctx.rect(x1, y1, x2 - x1, y2 - y1);
    ctx.stroke();
}

range.onchange = function () {
    lWidth = this.value;
};

eraser.onclick = function () {
    mode = "eraser";
    clear = true;
    input_text.style.display = "none";
    this.classList.add("active");
    brush.classList.remove("active");
    text.classList.remove("active");
    circle.classList.remove("active");
    triangle.classList.remove("active");
    rectangle.classList.remove("active");
};

brush.onclick = function () {
    mode = "drawline";
    input_text.style.display = "none";
    clear = false;
    this.classList.add("active");
    eraser.classList.remove("active");
    text.classList.remove("active");
    circle.classList.remove("active");
    triangle.classList.remove("active");
    rectangle.classList.remove("active");
};

text.onclick = function () {
    mode = "text";
    clear = false;
    this.classList.add("active");
    brush.classList.remove("active");
    eraser.classList.remove("active");
    circle.classList.remove("active");
    triangle.classList.remove("active");
    rectangle.classList.remove("active");
}

circle.onclick = function () {
    mode = "drawcircle";
    clear = false;
    input_text.style.display = "none";
    this.classList.add("active");
    brush.classList.remove("active");
    text.classList.remove("active");
    eraser.classList.remove("active");
    triangle.classList.remove("active");
    rectangle.classList.remove("active");
}

triangle.onclick = function () {
    mode = "drawtriangle";
    clear = false;
    input_text.style.display = "none";
    this.classList.add("active");
    brush.classList.remove("active");
    text.classList.remove("active");
    eraser.classList.remove("active");
    circle.classList.remove("active");
    rectangle.classList.remove("active");
}

rectangle.onclick = function () {
    mode = "drawrectangle";
    clear = false;
    input_text.style.display = "none";
    this.classList.add("active");
    brush.classList.remove("active");
    text.classList.remove("active");
    eraser.classList.remove("active");
    circle.classList.remove("active");
    triangle.classList.remove("active");
}

reSetCanvas.onclick = function () {
    ctx.clearRect(0, 0, canvas.width, canvas.height);
    input_text.style.display = "none";
    setCanvasBg('#ddd');
    this.firstDot = ctx.getImageData(0, 0, canvas.width, canvas.height); //在这里储存绘图表面
    saveData(this.firstDot);
};

save.onclick = function () {
    let imgUrl = canvas.toDataURL("image/png");
    let saveA = document.createElement("a");
    input_text.style.display = "none";
    document.body.appendChild(saveA);
    saveA.href = imgUrl;
    saveA.download = "Painter";
    saveA.target = "_blank";
    saveA.click();
};

function getColor() {
    activeColor = document.getElementById("colorful").value;
    ctx.strokeStyle = activeColor;
    ctx.fillStyle = activeColor;
}

function saveData(data) {
    (historyData.length === 10) && (historyData.shift()); // 上限為儲存10步
    historyData.push(data);
    undoData = [];
    //console.log("saved");
    //console.log(historyData.length);
}

function upload() {

    var img = new Image();

    img.onload = function () {
        var img_width = this.width;
        var img_height = this.height;
        var ratio1 = img_width / canvas.width;
        var ratio2 = img_height / canvas.height;
        console.log(img_height, img_width, canvas.height, canvas.width, ratio1, ratio2);

        if (ratio1 > ratio2 && ratio1 > 1) {
            img_width = img_width / ratio1;
            img_height = img_height / ratio1;
        } else if (ratio2 > ratio1 && ratio2 > 1) {
            img_height = img_height / ratio2;
            img_width = img_width / ratio2;
        }
        console.log(img_height, img_width, canvas.height, canvas.width);

        ctx.drawImage(this, 0, 0, img_width, img_height);
        URL.revokeObjectURL(src);
        getColor();
        this.firstDot = ctx.getImageData(0, 0, canvas.width, canvas.height); //在这里储存绘图表面
        saveData(this.firstDot);
    }

    var file = document.getElementById('upload').files[0];
    var src = URL.createObjectURL(file);

    img.src = src;

};

undo.onclick = function () {
    if (historyData.length <= 1) return false;
    undoData.push(historyData.pop());
    ctx.putImageData(historyData[historyData.length - 1], 0, 0);

    //console.log(historyData.length);
    //console.log(undoData.length);
};

redo.onclick = function () {
    if (undoData.length < 1) return false;
    historyData.push(undoData.pop());
    ctx.putImageData(historyData[historyData.length - 1], 0, 0);

    //console.log(undoData.length);
};